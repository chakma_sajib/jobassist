import React from 'react';
import Footer from '../components/shared/Footer';
import Header from '../components/shared/Header';
import SuperAdmin from '../components/SuperAdmin';

export default function AdminDashboard() {
  return (
    <div>
      <Header />
      <SuperAdmin />
    </div>
  );
}
