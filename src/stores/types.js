export const CREATE_HR = 'CREATE_HR';
export const RETRIEVE_HRS = 'RETRIEVE_HRS';
export const UPDATE_HR = 'UPDATE_HR';
export const DELETE_HR = 'DELETE_HR';
export const DELETE_ALL_HRS = 'DELETE_ALL_HRS';
export const COUNT_CHANGE = 'COUNTER_CHANGE';
export const GET_USERS = 'GET_USERS';
export const USERS_ERROR = 'USERS_ERROR';
