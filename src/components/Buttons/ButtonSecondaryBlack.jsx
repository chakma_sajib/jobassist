import React from 'react';

export default function ButtonSecondaryBlack({ text }) {
  return (
    <div>
      <button className='z-btn secondary black'>{text}</button>
    </div>
  );
}
