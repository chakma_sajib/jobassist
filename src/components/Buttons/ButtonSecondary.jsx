import React from 'react';

export default function ButtonSecondary({ text }) {
  return (
    <div>
      <button className='z-btn secondary'>{text}</button>
    </div>
  );
}
